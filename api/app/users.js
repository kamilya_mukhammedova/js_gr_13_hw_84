const express = require('express');
const User = require('../models/User');
const router = express.Router();

router.get('/', async (req, res, next) => {
  try {
    const users = await User.find();
    return res.send(users);
  } catch (e) {
    next(e);
  }
});

router.post('/', async (req, res, next) => {
  try {
    if (!req.body.name) {
      return res.status(400).send({message: 'User name is required!'});
    }

    const userData = {name: req.body.name};
    const newUser = new User(userData);
    await newUser.save();
    return res.send({message: 'Created new user', id: newUser._id});
  } catch (e) {
    next(e);
  }
});

module.exports = router;