const express = require('express');
const Task = require('../models/Task');
const router = express.Router();

router.get('/', async (req, res, next) => {
  try {
    const tasks = await Task.find().populate('user');
    return res.send(tasks);
  } catch (e) {
    next(e);
  }
});

router.post('/', async (req, res, next) => {
  try {
    if (!req.body.title) {
      return res.status(400).send({message: 'Title of task is required!'});
    }

    const taskData = {
      user: null,
      title: req.body.title,
      status: 'new',
    };

    if(req.body.user) {
      taskData.user = {_id: req.body.user};
    }

    const newTask = new Task(taskData);
    await newTask.save();

    return res.send({message: 'Created new task', id: newTask._id});
  } catch (e) {
    next(e);
  }
});

router.put('/:id', async (req, res, next) => {
  try {
    if(req.body.status) {
      const editedTask = await Task.updateOne({_id: req.params.id}, {$set: {status: req.body.status}});
      await editedTask.save();
    }

    if(req.body.user) {
      const editedTask = await Task.updateOne({_id: req.params.id}, {$set: {user: req.body.user}});
      await editedTask.save();
    }

    if(req.body.user === null) {
      const editedTask = await Task.updateOne({_id: req.params.id}, {$set: {user: null}});
      await editedTask.save();
    }

    return res.send({message: `Task with id ${req.params.id} has just been edited`});
  } catch (e) {
    next(e);
  }
});

router.delete('/:id', async (req, res, next) => {
  try {
    await Task.deleteOne({_id: req.params.id});
    return res.send({message: `Task with id ${req.params.id} has just been removed`});
  } catch (e) {
    next(e);
  }
});

module.exports = router;