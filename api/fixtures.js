const mongoose = require('mongoose');
const config = require("./config");
const User = require('./models/User');
const Task = require('./models/Task');

const run = async () => {
  await mongoose.connect(config.mongo.db, config.mongo.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for(const coll of collections) {
    await mongoose.connection.dropCollection(coll.name);
  }

  const [Jane, Jhon, Alex] = await User.create({
    name: 'Jane'
  }, {
    name: 'Jhon'
  }, {
    name: 'Alex'
  });

  await Task.create({
    user: Jane,
    title: 'Buy groceries',
    status: 'new'
  }, {
    user: Jhon,
    title: 'Go to car repairs',
    status: 'new'
  }, {
    user: Alex,
    title: 'Do homework',
    status: 'new'
  });

  await mongoose.connection.close();
};

run().catch(e => console.error(e));