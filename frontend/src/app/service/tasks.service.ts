import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiTaskData, Task } from '../models/task.model';
import { environment } from '../../environments/environment';
import { map } from 'rxjs';
import { ApiUserData, User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class TasksService {

  constructor(private http: HttpClient) { }

  getTasks() {
    return this.http.get<ApiTaskData[]>(environment.apiUrl + '/tasks').pipe(
      map(response => {
        if(response.length === 0) {
          return [];
        }
        return response.map(taskData => {
          return new Task(
            taskData._id,
            taskData.user,
            taskData.title,
            taskData.status,
          );
        });
      })
    );
  }

  removeTask(id: string) {
    return this.http.delete(environment.apiUrl + '/tasks/' + id);
  }

  getUsers() {
    return this.http.get<ApiUserData[]>(environment.apiUrl + '/users').pipe(
      map(response => {
        if(response.length === 0) {
          return [];
        }
        return response.map(userData => {
          return new User(
            userData._id,
            userData.name,
          );
        });
      })
    );
  }

}
