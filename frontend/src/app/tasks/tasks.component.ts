import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../store/types';
import { Observable } from 'rxjs';
import { Task } from '../models/task.model';
import { deleteTaskRequest, fetchTasksRequest } from '../store/tasks.actions';
import { User } from '../models/user.model';
import { fetchUsersRequest } from '../store/users.actions';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.sass']
})
export class TasksComponent implements OnInit {
  tasks: Observable<Task[]>;
  users: Observable<User[]>;
  loadingTasks: Observable<boolean>;
  errorTasks: Observable<null | string>;
  loadingUsers: Observable<boolean>;
  errorUsers: Observable<null | string>;
  removeLoading: Observable<boolean>;
  removeError: Observable<null | string>;

  constructor(private store: Store<AppState>) {
    this.tasks = store.select(state => state.tasks.tasks);
    this.loadingTasks = store.select(state => state.tasks.fetchLoading);
    this.errorTasks = store.select(state => state.tasks.fetchError);
    this.users = store.select(state => state.users.users);
    this.loadingUsers = store.select(state => state.users.fetchLoading);
    this.errorUsers = store.select(state => state.users.fetchError);
    this.removeLoading = store.select(state => state.tasks.removeLoading);
    this.removeError = store.select(state => state.tasks.removeError);
  }

  ngOnInit() {
    this.store.dispatch(fetchTasksRequest());
    this.store.dispatch(fetchUsersRequest());
  }

  onRemove(id: string) {
    this.store.dispatch(deleteTaskRequest({id}));
  }
}
