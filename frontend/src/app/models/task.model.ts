export class Task {
  constructor(
    public id: string,
    public user: null | string,
    public title: string,
    public status: string,
  ) {}
}

export interface TaskData {
  user: string;
  title: string;
  status: string;
}

export interface ApiTaskData {
  _id: string;
  user: null | string;
  title: string;
  status: string;
}
