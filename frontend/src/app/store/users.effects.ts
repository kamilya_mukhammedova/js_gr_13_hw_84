import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { TasksService } from '../service/tasks.service';
import { fetchUsersFailure, fetchUsersRequest, fetchUsersSuccess } from './users.actions';
import { catchError, map, mergeMap, of } from 'rxjs';

@Injectable()
export class UsersEffects {
  fetchUsers = createEffect(() => this.actions.pipe(
    ofType(fetchUsersRequest),
    mergeMap(() => this.tasksService.getUsers().pipe(
      map(users => fetchUsersSuccess({users})),
      catchError(() => of(fetchUsersFailure({
        error: 'Something went wrong with users list!'
      })))
    ))
  ));

  constructor(
    private actions: Actions,
    private tasksService: TasksService
  ) {}
}
