import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { TasksService } from '../service/tasks.service';
import {
  deleteTaskFailure,
  deleteTaskRequest,
  deleteTaskSuccess,
  fetchTasksFailure,
  fetchTasksRequest,
  fetchTasksSuccess
} from './tasks.actions';
import { catchError, map, mergeMap, of, tap } from 'rxjs';
import { Router } from '@angular/router';

@Injectable()
export class TasksEffects {
  fetchTasks = createEffect(() => this.actions.pipe(
    ofType(fetchTasksRequest),
    mergeMap(() => this.tasksService.getTasks().pipe(
      map(tasks => fetchTasksSuccess({tasks})),
      catchError(() => of(fetchTasksFailure({
        error: 'Something went wrong with tasks list!'
      })))
    ))
  ));

  removeTask = createEffect(() => this.actions.pipe(
    ofType(deleteTaskRequest),
    mergeMap(({id}) => this.tasksService.removeTask(id).pipe(
      map(() => deleteTaskSuccess()),
      tap(() => this.tasksService.getTasks().pipe(
        map(tasks => fetchTasksSuccess({tasks}))
      )),
      catchError(() => of(deleteTaskFailure({error: 'Something went wrong with removing task!'})))
    ))
  ));

  constructor(
    private actions: Actions,
    private tasksService: TasksService,
    private router: Router
    ) {}
}
