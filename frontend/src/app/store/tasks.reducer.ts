import { TasksState } from './types';
import { createReducer, on } from '@ngrx/store';
import {
  deleteTaskFailure,
  deleteTaskRequest,
  deleteTaskSuccess,
  fetchTasksFailure,
  fetchTasksRequest,
  fetchTasksSuccess
} from './tasks.actions';

const initialState: TasksState = {
  tasks: [],
  fetchLoading: false,
  fetchError: null,
  removeLoading: false,
  removeError: null,
};

export const tasksReducer = createReducer(
  initialState,
  on(fetchTasksRequest, state => ({...state, fetchLoading: true})),
  on(fetchTasksSuccess, (state, {tasks}) => ({
    ...state,
    fetchLoading: false,
    tasks
  })),
  on(fetchTasksFailure, (state, {error}) => ({
    ...state,
    fetchLoading: false,
    fetchError: error
  })),
  on(deleteTaskRequest, state => ({...state, removeLoading: true})),
  on(deleteTaskSuccess, state => ({...state, removeLoading: false})),
  on(deleteTaskFailure, (state, {error}) => ({
    ...state,
    removeLoading: false,
    removeError: error
  }))
);
